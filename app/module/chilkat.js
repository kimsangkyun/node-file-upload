﻿
module.exports.initOnOs = function () {
  var os = require('os');
  var chilkat;
  if (os.platform() == 'win32') {
    chilkat = require('chilkat_win32');
  } else if (os.platform() == 'linux') {
    if (os.arch() == 'x86') {
      chilkat = require('chilkat_linux32');
    } else {
      chilkat = require('chilkat_linux64');
    }
  } else if (os.platform() == 'darwin') {
    chilkat = require('chilkat_macosx');
  }

  return new chilkat.Zip();
};