﻿'user strict'

module.exports.upload = function (req, res, deleteFileList) {
  var multer = require('multer');
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'upload/images')
    },
    filename: function (req, file, cb) {
      var ext = file.mimetype.split('/')[1];
      var filename = file.originalname.split('.')[0] + '-' + Date.now() + '.' + ext;
      cb(null, filename);
    }
  });

  var upload = multer({ storage: storage }).single('imageFile');

  return function (callback) {
    upload(req, res, function (err) {
      if (err) callback(err);
      else {
        var filename = req.file.filename;
        if (!req.file.mimetype.match(/image\//)) {//validation.js를 만드는 것이 좋음
          callback(new error('image가 아닙니다.'));
          return;
        }
        deleteFileList.push(deletePath(filename));
        callback(null, filename, {
          req: req,
          x: 200,
          y: 200,
          recur: true,
          deleteFileList: deleteFileList
        });
      }
    });
  };
}

module.exports.resizeImage = function (filename, obj, callback) {
  
  var gm = require('gm').subClass({ imageMagick: true });
  var uploadPath = __dirname + '/../../upload/images/';
  var imageGm = gm(uploadPath + filename);//이미지를 불러옴
  var x = obj.x;
  var y = obj.y;
  var filenameSplit = filename.split('.');
  var ext = '.' + filenameSplit[1];
  var resizeFilename = filenameSplit[0] + '-' + x + ext;//~~/filename-200.jpg
  var fullPathFilename = uploadPath + resizeFilename; 
  var deleteFileList = obj.deleteFileList;
  var req = obj.req;

  imageGm
  .resizeExact(x, y)
  .write(fullPathFilename, function (err) {
    if (err) callback(new Error('이미지 리사이징 error'));
    else if (obj.recur) {
      deleteFileList.push(deletePath(resizeFilename));
      callback(null, filename, { req: req, x: 400, y: 400, deleteFileList: deleteFileList });
    } else {
      deleteFileList.push(deletePath(resizeFilename));
      callback(null, filename, req.body.zipPassword);
    }
  });
  
}

function deletePath(filename) { 
  return 'upload/images/' + filename;
}