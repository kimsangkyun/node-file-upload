﻿'use strict'

module.exports.zipSecret = function (filename, zipPassword, callback) {
  
  var zip = require('./chilkat.server.module').initOnOs();
  var success = zip.UnlockComponent("Anything for 30-day trial");
  var filenameSplit = filename.split('.');
  var fullPath = __dirname + '/../../upload/images/';
  var filenameExceptExt = filenameSplit[0];
  var ext = filenameSplit[1];
  var zipFilename = filenameExceptExt + ".zip";
    //  Any string unlocks the component for the 1st 30-days.

  if (!success) {
    callback(new Error(zip.LastErrorText));
    return;
  }
  
  success = zip.NewZip(fullPath + zipFilename);
  if (!success) {
    callback(new Error(zip.LastErrorText));
    return;
  }
    
  zip.SetPassword(zipPassword);
  zip.PasswordProtect = true;
  
  var saveExtraPath = false;
  var findPatterFile = fullPath + filenameExceptExt + "-*." + ext;
  
  success = zip.AppendFiles(findPatterFile, saveExtraPath);
  zip.SetCompressionLevel(7);
  var success = zip.WriteZipAndClose();
  if (!success) {
    callback(new Error(zip.LastErrorText));
    return;
  }
  
  callback(null, {zipFilename: zipFilename, filename: filename});
}