﻿'use strict';

module.exports = function (app) {
  // Root routing
  var image = require('../../app/controllers/image.server.controller');
  
  app.route('/image')
     .post(image.create);
  app.route('/image/:imageName/view')
     .get(image.view);
  app.route('/image/:zipFilename')
     .get(image.read);
  
};