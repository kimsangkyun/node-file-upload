﻿'use strict';

/**
 * Module dependencies.
 */

var fs = require('fs');

exports.read = function (req, res) {
    res.download('./upload/images/' + req.params.zipFilename);
}

//s3리가 없어서 이렇게 할 수 밖에..
exports.view = function (req, res) {
    var imageName = req.params.imageName;
    
    fs.readFile('./upload/images/' + imageName, function (error, data) {
        var ext = imageName.split('.')[1];
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.end(data);
    });
}

exports.create = function (req, res) {
    var image = require('../module/image.server.module');
    var zip = require('../module/zip.server.module');
    var async = require('async');
    var del = require('del');
    var fs = require('fs');
    var deleteFileList = [];//만약 파일이 다 만들어 지지 않는다면 지우기 위한 배열
    
    //image를 업로드 하고 리사이즈를 한 후 압축 함
    async.waterfall([
        image.upload(req, res, deleteFileList),
        image.resizeImage,
        image.resizeImage,
        zip.zipSecret
    ],
  function done(err, results) {
        if (err) {
            del(deleteFileList, { dry: true }).then(function (paths) {
                return res.status(500).json({ status: 500, err: err });
            });
      
        } else {
            return res.status(200).json({ status: 200, results: results });
        }
    });

}