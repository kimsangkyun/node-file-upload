'use strict';

/**
 * Module dependencies.
 */

var fs = require('fs');

exports.index = function(req, res) {
	res.render('index', {
		user: req.user || null,
		request: req
	});
};


